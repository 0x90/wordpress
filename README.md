# Update apps

You can use the `update-app.sh` script to update an app, e.g. like this:

```
./update-app.sh akismet
```

To update all apps at once, use this command:

```
find ../wp-content/plugins/ -maxdepth 1 -type d | xargs -L1 basename | xargs -L1 ./update-app.sh
```

