from io import StringIO
import importlib.resources

from pyinfra.operations import files, systemd, server


def deploy_wordpress(
        unix_user: str,
        domain: str,
        wordpress_version: str,
        wordpress_port: int,
        mysql_ip: str,
        mysql_port: int,
        mysql_password: str = None,
        mysql_root_password: str = None,
        _su: dict = {},
    ):
    if not mysql_root_password:
        mysql_root_password = mysql_password
    # check that podman is installed
    secrets = [
        f"MYSQL_PASSWORD={mysql_password}",
        f"MYSQL_ROOT_PASSWORD={mysql_root_password}",
    ]
    env = "\n".join(secrets)
    files.put(
        name="upload secrets",
        src=StringIO(env),
        dest=f"/home/{unix_user}/.env",
        mode="0600",
        user=unix_user,
    )

    # is this also possible with a config file and file.line()?
    server.shell(
        name="set firewall rules",
        commands=[
            "ufw default deny incoming",
            "ufw default allow outgoing",
            "ufw allow 443",
            "ufw allow 80",
            "ufw allow 22",
            "ufw allow 42022",
            "ufw status",
            # "ufw reload?",
        ],
    )
    files.directory(
        name="create db_data directory",
        path=f"/home/{unix_user}/db_data",
        **_su,
    )
    files.directory(
        name="create wp-content directory",
        path=f"/home/{unix_user}/wp-content",
        **_su,
    )

    print(importlib.resources.files(__package__))
    service_path = f"/home/{unix_user}/.config/systemd/user/"
    environment = [
        f"WORDPRESS_DB_HOST={mysql_ip}:{mysql_port}",
        f"WORDPRESS_DB_PASSWORD={mysql_password}",
        "WORDPRESS_DB_USER=wordpress",
        "WORDPRESS_DB_NAME=wordpress",
    ]
    mount = [
        f"type=bind,source=/home/{unix_user}/wp-content,destination=/var/www/html/wp-content",
        f"type=bind,source=/home/{unix_user}/uploads.ini,destination=/usr/local/etc/php/conf.d/uploads.ini",
    ]
    files.template(
        name="upload wordpress systemd service",
        src=importlib.resources.files(__package__) / "podman.service.j2",
        dest=f"{service_path}{domain}_wp.service",
        description="run wordpress podman container",
        container_name=f"{domain}_wp",
        environment="-e " + " -e ".join(environment),
        mount="--mount " + " --mount ".join(mount),
        port=f"-p 127.0.0.1:{wordpress_port}:80",
        container="wordpress",
        version=wordpress_version,
        **_su,
    )

    environment = [
        f"MYSQL_ROOT_PASSWORD={mysql_password}",
        f"MYSQL_PASSWORD={mysql_password}",
        "MYSQL_DATABASE=wordpress",
        "MYSQL_USER=wordpress",
    ]
    files.template(
        name="upload mysql systemd service",
        src=importlib.resources.files(__package__) / "podman.service.j2",
        dest=f"{service_path}{domain}_db.service",
        description="run mysql podman container",
        container_name=f"{domain}_db",
        environment="-e " + " -e ".join(environment),
        mount=f"--mount type=bind,source=/home/{unix_user}/db_data,destination=/var/lib/mysql",
        port=f"-p {mysql_port}:3306",
        container="mysql",
        version="5.7",
        **_su,
    )

    files.put(
        name="upload uploads.ini",
        src=importlib.resources.files(__package__) / "uploads.ini",
        dest=f"/home/{unix_user}/uploads.ini",
        **_su,
    )

    for container in ["db", "wp"]:
        systemd.service(
            name=f"restart {domain}_{container} service",
            service=f"{domain}_{container}.service",
            enabled=True,
            running=True,
            restarted=True,
            daemon_reload=True,
            user_name=unix_user,
            user_mode=True,
            **_su,
        )
