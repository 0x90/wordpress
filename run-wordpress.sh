#!/bin/sh

# include environment variables
. ./.env
# stop and remove old containers
podman rm -f wordpress_db_1 wordpress_wordpress_1  

# run db container
podman run --name=wordpress_db_1 --detach --pull always \
        -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} 	\
        -e MYSQL_DATABASE=wordpress 			\
        -e MYSQL_USER=wordpress 			\
        -e MYSQL_PASSWORD=${MYSQL_PASSWORD} 		\
        --mount type=bind,source=/home/${USER}/db_data/_data/,destination=/var/lib/mysql \
        -p ${MYSQL_PORT}:3306 \
        mysql:5.7

# run wordpress container
podman run --name=wordpress_wordpress_1 --detach --pull always \
        -e WORDPRESS_DB_HOST=78.46.128.202:${MYSQL_PORT} 	\
        -e WORDPRESS_DB_USER=wordpress 			\
        -e WORDPRESS_DB_PASSWORD=${MYSQL_PASSWORD}	\
        -e WORDPRESS_DB_NAME=wordpress 			\
        --mount type=bind,source=/home/${USER}/wp-content,destination=/var/www/html/wp-content \
        --mount type=bind,source=/home/${USER}/wordpress/uploads.ini,destination=/usr/local/etc/php/conf.d/uploads.ini \
        -p 127.0.0.1:${NGINX_PORT}:80 \
        wordpress:${WORDPRESS_VERSION}

# show running containers
podman ps
podman logs -ft wordpress_wordpress_1
