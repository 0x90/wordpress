#!/bin/bash
set -xeou pipefail

# setup env
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
. ./.env

# check if these variables have been set
echo "$BORG_REPO" > /dev/null
echo "$BORG_PASSPHRASE" > /dev/null
echo "$FOLDERS_TO_BACKUP" > /dev/null
echo "$POD_NAMES" > /dev/null

borg check

# don't fail just because the pod is not running
set +e
podman stop ${POD_NAMES}
set -e

podman unshare borg create --stats ::'backup{now:%Y%m%d-%M}' ${FOLDERS_TO_BACKUP}

borg list

podman start ${POD_NAMES}
