#!/bin/sh

# fail script if one command fails
set -e

# check file permissions
if [ ! -w ~/wp-content/plugins ]
  then
	echo "Missing file permissions. To fix this, run this command with podman unshare:"
	echo "  podman unshare ./update-app.sh $1"
	exit 0
fi

# we need at least one argument
if [ ! $# -eq 1 ]
  then
	echo "usage: podman unshare ./update-app.sh \$app"
	exit 0
fi
APP=$1

# download app
cd ~/wp-content/plugins
wget https://downloads.wordpress.org/plugin/$APP.zip

# install app
rm -rf $APP
unzip $APP.zip

# clean up
rm $APP.zip

